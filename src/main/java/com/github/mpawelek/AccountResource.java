package com.github.mpawelek;

import com.github.mpawelek.concurrent.Cancelable;
import com.github.mpawelek.concurrent.ConcurrentRunner;
import com.github.mpawelek.concurrent.ee.EEConcurrentRunner;
import com.github.mpawelek.concurrent.ee.tx.Tx;
import com.github.mpawelek.concurrent.source.ListCancelableSource;
import com.github.mpawelek.equality.EqualableInt;
import com.github.mpawelek.equality.EqualableString;
import com.github.mpawelek.locking.ResourceMutex;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("account")
public class AccountResource {
    @PersistenceContext
    private EntityManager em;

    @Resource
    private ManagedExecutorService thrPool;

    @Inject private EEConcurrentRunner ejbRunner;
    @Inject @Produced private ResourceMutex<EqualableString, EqualableInt> mutex;
    @Inject @Produced private ConcurrentRunner runner;
    @Inject private Tx tx;

    @GET @Path("new") @Produces(MediaType.APPLICATION_JSON)
    public AccountEntity newAccount() {
        AccountEntity a=new AccountEntity();
        a.setAmount(100);
        em.persist(a);
        return a;
    }

    @GET @Path("test") @Produces(MediaType.TEXT_PLAIN)
    public String test() throws InterruptedException, ErrorException {
        EqualableString lockedObj=new EqualableString("mutex");
        List<AccountEntity> e=new ArrayList<>();
        List<Runnable> tasks=new ArrayList<>();
        for (int i=0; i < 5; i++) {
            final int amount=i;
            final EqualableInt user=new EqualableInt(i);
            tasks.add(()->{
                try {
                    mutex.lock(lockedObj, user);
                    try {
                        AccountEntity a=new AccountEntity();
                        a.setAmount(amount);
                        tx.required(em-> { em.persist(a); em.flush(); /* throw new ErrorException("test"); */ });
                        System.out.println("persist "+a.getId());
                        e.add(a);
                        Thread.sleep(1000);
                    } finally {
                        mutex.unlock(lockedObj, user);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
        }
        Cancelable c=ejbRunner.start(new ListCancelableSource(tasks), 5, System.out);
        c.await();
        for (AccountEntity a: e) {
            System.out.println(a.getId()+": "+em.contains(a));
        }
//        if (true) throw new ErrorException("test");
        return "ok";
    }

    @GET @Path("test2") @Produces(MediaType.APPLICATION_JSON)
    public AccountEntity test2() throws Exception {
        AccountEntity a=new AccountEntity();
        a.setAmount(100);
        tx.required(em->{
            em.persist(a);
            em.flush();
            System.out.println("persist 2: "+a.getId());
        });
        System.out.println("persist 1: "+a.getId()+", "+em.contains(a));
//        if (true) throw new ErrorException("test");
        return a;
    }
}
