package com.github.mpawelek;

import com.github.mpawelek.concurrent.ConcurrentRunner;
import com.github.mpawelek.equality.EqualableInt;
import com.github.mpawelek.equality.EqualableString;
import com.github.mpawelek.locking.ResourceMutex;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class BeanProducer {
    @Resource
    private ManagedExecutorService e;

    @Produces @Produced @ApplicationScoped
    public ResourceMutex<EqualableString, EqualableInt> resourceMutex() {
        return new ResourceMutex<>();
    }

    @Produces @Produced @ApplicationScoped
    public ConcurrentRunner concurrentRunner() {
        return new ConcurrentRunner(e);
    }
}
