package com.github.mpawelek;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class Database {
    @PersistenceContext
    private EntityManager em;

    public <T> T persist(T e) {
        em.persist(e);
        return e;
    }

    public <T> T merge(T e) {
        return em.merge(e);
    }
}
