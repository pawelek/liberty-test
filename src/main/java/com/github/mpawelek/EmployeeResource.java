package com.github.mpawelek;

import com.github.mpawelek.concurrent.ee.tx.Tx;
import com.github.mpawelek.entities.Address;
import com.github.mpawelek.entities.ContactInfo;
import com.github.mpawelek.entities.Employee;
import com.github.mpawelek.entities.PhoneNumber;

import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("emp") @RequestScoped
public class EmployeeResource {
    @PersistenceContext
    private EntityManager em;

    @GET @Path("test") @Produces(MediaType.TEXT_PLAIN)
    @Transactional
    public String test(@QueryParam("phone") Long phone) throws Exception {
        Address addr=new Address();
        addr.setStreet("Piastowska");
        addr.setHouseNo(24);
        addr.setFlatNo(4);

        PhoneNumber pn1=new PhoneNumber();
        pn1.setNumber(phone);

        ContactInfo ci=new ContactInfo();
        ci.setAddress(addr);
        ci.addPhoneNumber(pn1);

        Employee emp=new Employee();
        emp.setContactInfo(ci);

        em.persist(emp);

        return "ok";
    }
}
