package com.github.mpawelek;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class ErrorException extends Exception {
    public ErrorException(String error) {
        super(error);
    }
    public ErrorException(Throwable error) {
        super(error);
    }
}
