package com.github.mpawelek;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@WebServlet(urlPatterns = {"/", "/static"})
public class FileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse reply) throws IOException {
        String path=req.getServletPath().replaceAll("/+", "/");
        switch (path) {
            case "/":
                reply.setContentType(MediaType.TEXT_HTML);
                reply.getWriter().write(serveResource(path+"index.html"));
                break;
            default:
                String contentType=getServletContext().getMimeType(path);
                if (contentType != null) {
                    reply.setContentType(contentType);
                } else {
                    reply.setContentType(MediaType.TEXT_PLAIN);
                }
                reply.getWriter().write(serveResource(path));
                break;
        }
    }

    private String serveResource(String httpPath) throws IOException {
        String path="/ui"+httpPath.replaceAll("/+", "/");
        InputStream r = getClass().getResourceAsStream(path);
        StringBuilder s=new StringBuilder();
        try {
            byte[] b = new byte[1024];
            for (; ; ) {
                int n = r.read(b);
                if (n < 1) {
                    break;
                }
                s.append(new String(b, 0, n, StandardCharsets.UTF_8));
            }
            return s.toString();
        } finally {
            r.close();
        }
    }
}
