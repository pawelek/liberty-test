package com.github.mpawelek;

import com.github.mpawelek.entities.VertexEntity;
import com.github.mpawelek.entities.WeightedEdgeEntity;
import com.github.mpawelek.entities.WeightedVertexEntity;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("graph") @RequestScoped
public class GraphResource {
    @PersistenceContext
    private EntityManager em;

    /*
    @GET @Path("add") @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public String add() throws Exception {
        VertexEntity v=new VertexEntity();
        v.setName("v");
        em.persist(v);

//        VertexEntity s1=new VertexEntity();
//        s1.setName("s1");
//        em.persist(s1);
//
//        VertexEntity s2=new VertexEntity();
//        s2.setName("s2");
//        em.persist(s2);

        VertexEntity t1=new VertexEntity();
        t1.setName("t1");
        em.persist(t1);

//        VertexEntity t2=new VertexEntity();
//        t2.setName("t2");
//        em.persist(t2);

//        v.addSource(s1, 1);
//        v.addSource(s2, 2);
        v.addTarget(t1);
        t1.addTarget(v);
//        v.addTarget(t2, 4);

        return "ok";
    }
    */

    @GET @Path("add2") @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public String add2() {
        WeightedVertexEntity v=new WeightedVertexEntity();
        v.setName("v");
        em.persist(v);

        WeightedVertexEntity t=new WeightedVertexEntity();
        t.setName("t");
        em.persist(t);

        v.addTarget(new WeightedEdgeEntity(t, 10));

        return "ok";
    }

    /*
    @GET @Path("get") @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public VertexEntity get(@QueryParam("id") Long vertexId) throws Exception {
        VertexEntity v=em.find(VertexEntity.class, vertexId);
        for (AbstractEdgeEntity s: v.getSources()) {
            System.out.println("src="+s.getVertex().getName()+", "+s.getVertex().getId());
        }
        for (AbstractEdgeEntity t: v.getTargets()) {
            System.out.println("trg="+t.getVertex().getName()+", "+t.getVertex().getId());
        }
        return v;
    }
    */

    @GET @Path("get2") @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public VertexEntity get2(@QueryParam("id") Long vertexId) throws Exception {
        WeightedVertexEntity v=em.find(WeightedVertexEntity.class, vertexId);
        for (WeightedEdgeEntity s: v.getSources()) {
            System.out.println("src="+s.getVertex().getName()+", "+s.getVertex().getId()+", "+s.getWeight());
        }
        for (WeightedEdgeEntity t: v.getTargets()) {
            System.out.println("trg="+t.getVertex().getName()+", "+t.getVertex().getId()+", "+t.getWeight());
        }
        return v;
    }
}
