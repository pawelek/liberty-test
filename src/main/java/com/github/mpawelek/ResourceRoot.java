package com.github.mpawelek;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class ResourceRoot extends Application {
}
