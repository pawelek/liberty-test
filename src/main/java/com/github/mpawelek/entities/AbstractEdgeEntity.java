package com.github.mpawelek.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;

/*
    CREATE TABLE edge (
        id BIGSERIAL PRIMARY KEY,
        vertex_id BIGINT REFERENCES vertex(id),
        weight INT,
        version BIGINT
    );
 */
@Entity @Table(name="edge")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractEdgeEntity {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Long version;

    @ManyToOne @JoinColumn(name="vertex_id")
    @JsonBackReference
    protected VertexEntity vertex;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public VertexEntity getVertex() {
        return vertex;
    }

    public void setVertex(VertexEntity vertex) {
        this.vertex = vertex;
    }

    public abstract AbstractEdgeEntity inverse(VertexEntity anchor);
}
