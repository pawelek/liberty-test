package com.github.mpawelek.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Embeddable
public class ContactInfo {
    @Embedded
    @AttributeOverride(name="street", column=@Column(name="addr_street"))
    @AttributeOverride(name="houseNo", column = @Column(name="addr_houseno"))
    @AttributeOverride(name="flatNo", column=@Column(name="addr_flatno"))
    private Address address;

    @ManyToMany(cascade = CascadeType.ALL) @JoinTable(name="employee_phones",
            joinColumns = @JoinColumn(name="employee_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name="phone_number", referencedColumnName = "number")
    )
    private List<PhoneNumber> phones;

    public ContactInfo() {
        phones=new ArrayList<>();
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<PhoneNumber> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneNumber> phones) {
        this.phones = phones;
    }

    public void addPhoneNumber(PhoneNumber phoneNumber) {
        phones.add(phoneNumber);
    }
}
