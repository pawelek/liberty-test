package com.github.mpawelek.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/*
    create table phone_number (
        number bigint primary key
    );
 */
@Entity @Table(name="phone_number")
public class PhoneNumber {
    @Id
    private Long number;

    @ManyToMany(mappedBy = "contactInfo.phones")
    private List<Employee> employees;

    public PhoneNumber() {
        employees=new ArrayList<>();
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
