package com.github.mpawelek.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;

/*
    CREATE TABLE vertex (
        id BIGSERIAL PRIMARY KEY,
        name TEXT,
        version BIGINT
    );

    CREATE TABLE source_edge (
        vertex_id BIGINT REFERENCES vertex(id),
        edge_id BIGINT REFERENCES edge(id)
    );

    CREATE TABLE target_edge (
        vertex_id BIGINT REFERENCES vertex(id),
        edge_id BIGINT REFERENCES edge(id)
    );
 */
@Entity @Table(name="vertex")
@Inheritance(strategy = InheritanceType.JOINED)
public class VertexEntity<T extends AbstractEdgeEntity> {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Version
    private Long version;

    @ManyToMany(targetEntity = AbstractEdgeEntity.class, cascade = CascadeType.ALL) @JoinTable(name="target_edge",
            joinColumns = @JoinColumn(name="vertex_id"),
            inverseJoinColumns = @JoinColumn(name="edge_id")
    )
    @JsonManagedReference
    private List<T> targets;

    @ManyToMany(targetEntity = AbstractEdgeEntity.class, cascade = CascadeType.ALL) @JoinTable(name="source_edge",
            joinColumns = @JoinColumn(name="vertex_id"),
            inverseJoinColumns = @JoinColumn(name="edge_id")
    )
    @JsonManagedReference
    private List<T> sources;

    public VertexEntity() {
        sources=new ArrayList<>();
        targets=new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public List<T> getTargets() {
        return targets;
    }

    public void setTargets(List<T> targets) {
        this.targets = targets;
    }

    public List<T> getSources() {
        return sources;
    }

    public void setSources(List<T> sources) {
        this.sources = sources;
    }

    public void addSource(T sourceEdge) {
        sources.add(sourceEdge);

        T targetEdge= (T) sourceEdge.inverse(this);
        sourceEdge.getVertex().getTargets().add(targetEdge);
    }

    public void addTarget(T targetEdge) {
        targets.add(targetEdge);

        T sourceEdge= (T) targetEdge.inverse(this);
        targetEdge.getVertex().getSources().add(sourceEdge);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
