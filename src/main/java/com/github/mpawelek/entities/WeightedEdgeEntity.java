package com.github.mpawelek.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

/*
    CREATE TABLE WEIGHTED_EDGE (
        id BIGSERIAL PRIMARY KEY,
        weight INT
    );
*/
@Entity @Table(name="weighted_edge")
public class WeightedEdgeEntity extends AbstractEdgeEntity {
    private Integer weight;

    public WeightedEdgeEntity() {}

    public WeightedEdgeEntity(VertexEntity anchor, int weight) {
        this.vertex=anchor;
        this.weight=weight;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override public AbstractEdgeEntity inverse(VertexEntity anchor) {
        WeightedEdgeEntity e=new WeightedEdgeEntity();
        e.setVertex(anchor);
        e.setWeight(weight);
        return e;
    }
}
