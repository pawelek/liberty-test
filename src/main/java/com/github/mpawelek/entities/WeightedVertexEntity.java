package com.github.mpawelek.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

/*
    CREATE TABLE weighted_vertex (
        id BIGINT PRIMARY KEY
    );
 */
@Entity @Table(name="weighted_vertex")
public class WeightedVertexEntity extends VertexEntity<WeightedEdgeEntity> {
}
